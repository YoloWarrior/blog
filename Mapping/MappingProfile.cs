﻿using AutoMapper;
using Bll.DTOs;
using Data.Entity;
using Mapping.DTOs;
using Mapping.ViewModel;
using System;
using System.Collections.Generic;
using TzAPI.ViewModel;

namespace Mapping
{
    public  class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<TagViewModel, TagDTO>();
            CreateMap<UserViewModel,UserDTO>();
            CreateMap<UserDTO, User>();
            CreateMap<User, UserDTO>();
            CreateMap<TagDTO, TagViewModel>();
            CreateMap<ArticleIcon,IconDTO>();
            CreateMap<IconDTO, ArticleIconViewModel>();
            CreateMap<Category, CategoryDTO>();
            CreateMap<CategoryViewModel, CategoryDTO>();
            CreateMap<Role, RoleDTO>();
            CreateMap<Tag, TagDTO>();
            CreateMap<RoleDTO, Role>();
            CreateMap<TagDTO, Tag>()
                .ForMember(x => x.TagID, opt => opt.MapFrom(x => Guid.NewGuid()));
            CreateMap<ArticleDTO, ArticleViewModel>()
                .ForMember(x => x.Icon, opt => opt.MapFrom(x => x.ArticleIcon))
                .ForMember(x => x.Tags, opt => opt.MapFrom(x => x.Tags));


            CreateMap<CategoryDTO, CategoryViewModel>()
                .ForMember(x => x.Articles, opt => opt.MapFrom(x => x.Articles));


            CreateMap<CategoryViewModel, CategoryDTO>()
                .ForMember(x => x.Articles, opt => opt.MapFrom(x => x.Articles));
            CreateMap<ArticleViewModel, ArticleDTO>()
                .ForMember(x => x.IconID, opt => opt.MapFrom(x => x.IconID));


            CreateMap<Article, ArticleDTO>()
                 .ForMember(x => x.IconID, opt => opt.MapFrom(x => x.IconID))
                 .ForMember(x => x.ArticleIcon, opt => opt.MapFrom(x => x.ArticleIcon))
                 .ForMember(x => x.CategoryName, opt => opt.MapFrom(x => x.Category.Name));

            CreateMap<ArticleDTO, Article>()
                .ForMember(x => x.PublishDate, opt => opt.MapFrom(x => DateTime.Now))
                  .ForMember(x => x.IconID, opt => opt.MapFrom(x => x.IconID));

            CreateMap<CategoryDTO, Category>()
                .ForMember(x => x.ID ,opt => opt.MapFrom(x => Guid.NewGuid()));


            CreateMap<CategoryDTO, CategoryViewModelIenum>()
    .ForMember(x => x.CategoryID, opt => opt.MapFrom(x => x.ID))
    .ForMember(x => x.CategoryName, opt => opt.MapFrom(x => x.Name));

            CreateMap<ArticleViewModelForCreate, ArticleDTO>()
                     .ForMember(x => x.ArticleID, opt => opt.MapFrom(x => Guid.NewGuid()))
                .ForMember(x => x.Category, opt => opt.MapFrom(x => x.Category))
                 .ForMember(x => x.ArticleIcon, opt => opt.MapFrom(x => x.Icon))
                  .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                   .ForMember(x => x.ShortDescription, opt => opt.MapFrom(x => x.ShortDescription))
                    .ForMember(x => x.Tags, opt => opt.MapFrom(x => x.Tags))
                    .ForMember(x => x.Category, opt => opt.MapFrom(x=>x.Category));
        }
    }
}
