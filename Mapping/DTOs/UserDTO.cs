﻿using Mapping.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bll.DTOs
{
    public class UserDTO
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public RoleDTO RoleDTO { get; set; }
        public Guid RoleID { get; set; }

    }
}
