﻿using Bll.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mapping.DTOs
{
  public class CategoryDTO
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public virtual List<ArticleDTO> Articles { get; set; }
    }
}
