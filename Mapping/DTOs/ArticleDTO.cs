﻿using Mapping.DTOs;
using Mapping.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bll.DTOs
{
   public class ArticleDTO
    {
        public Guid ArticleID { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Text { get; set; }
        public string CategoryName { get; set; }
        public  DateTime PublishDate { get; set; }
        public CategoryDTO Category { get; set; }
        public Guid? IconID { get; set; }
        public virtual IconDTO ArticleIcon { get; set; }
        public virtual IEnumerable<TagDTO> Tags { get; set; }
    }
}
