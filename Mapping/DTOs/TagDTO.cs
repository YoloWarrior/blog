﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bll.DTOs
{
   public class TagDTO
    {
        public Guid TagID { get; set; }
        public string Name { get; set; }
    }
}
