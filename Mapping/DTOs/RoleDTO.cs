﻿using Bll.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mapping.DTOs
{
  public  class RoleDTO
    {
        public Guid RoleID { get; set; }
        public string RoleName { get; set; }
        public virtual List<UserDTO> User { get; set; }
    }
}
