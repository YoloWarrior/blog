﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bll.DTOs
{
    public class IconDTO
    {
        public Guid IconID { get; set; }
        public string Path { get; set; }
        public Guid ArticleID { get; set; }
        public ArticleDTO Article { get; set; }
    }
}
