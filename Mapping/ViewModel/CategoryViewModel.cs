﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mapping.ViewModel
{
 public   class CategoryViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public List<ArticleViewModel> Articles { get; set; }
    }
}
