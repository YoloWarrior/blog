﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mapping.ViewModel
{
  public  class ArticleViewModel
    {
        public Guid ArticleID { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string CategoryName { get; set; }
        public string Text { get; set; }
        public Guid? IconID { get; set; }
        public ArticleIconViewModel Icon { get; set; }
        public IEnumerable<TagViewModel> Tags { get; set; }

    }
   public class ArticleViewModelForCreate
    {
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Text { get; set; }

        public ArticleIconViewModel Icon { get; set; }
        public CategoryViewModel Category { get; set; }
        public IEnumerable<TagViewModel> Tags { get; set; }

    }
}
