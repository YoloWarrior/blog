﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TzAPI.ViewModel
{
    public class UserViewModel
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

    }
    public class UserViewModelSummary:UserViewModel {
        public string UserInf { get; set; }
    }
    public class UserViewModelForLogin : UserViewModel
    {
        public string Password { get; set; }
    }
        
}
