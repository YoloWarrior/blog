﻿
using Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace API.Helper
{
	public class AuthFilter : ActionFilterAttribute, IActionFilter
	{
		private DataContext _context = new DataContext();
		string _role;

		public AuthFilter(string role)
		{
			_role = role;
		}

		public void OnActionExecuted(ActionExecutedContext context)
		{

		}

		public override void OnActionExecuting(ActionExecutingContext context)
		{
			var p = context.HttpContext.Request.Headers["Authorization"];
			var handler = new JwtSecurityTokenHandler();
			dynamic jsonToken = "";
			var isAccess = false;

			try
			{

				var tokenS = handler.ReadToken(p) as JwtSecurityToken;
				isAccess = _context.Users.FirstOrDefault(x => x.UserName == tokenS.Claims.First(y => y.Type == "unique_name").ToString()
			   && x.Role.RoleName == _role) != null;
			}
			catch
			{
				context.Result = new StatusCodeResult(403);

			}

			if (!isAccess)
			{
				context.Result = new StatusCodeResult(403);
			}

		}
	}
}
