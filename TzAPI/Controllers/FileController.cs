﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Helper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class FileController : ControllerBase
    {
        IHostingEnvironment _appEnvironment;

        public FileController(IHostingEnvironment appEnvironment)
        {
           
            _appEnvironment = appEnvironment;
        }

        // GET: /<controller>/
        [HttpPost]
        public async Task<IActionResult> Post(IFormFile file)
        {
            //if (uploadedFile != null)
            //{
            //    // путь к папке Files
            //    string path = "/Files/" + uploadedFile.FileName;
            //    // сохраняем файл в папку Files в каталоге wwwroot
            //    using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
            //    {
            //        await uploadedFile.CopyToAsync(fileStream);
            //    }
            //}

            return Ok();
        }
    }
}
