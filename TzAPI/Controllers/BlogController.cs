﻿using API.Helper;
using AutoMapper;
using Bll.DTOs;
using Bll.Interfaces;
using Bll.Services;
using Mapping.DTOs;
using Mapping.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BlogController : ControllerBase
	{
		private readonly ICategory _category;
		private readonly IMapper _mapper;
		private readonly IArticle _article;
		private readonly IIcon _icon;
		private readonly ITag _tag;
		public BlogController(ICategory category, IMapper mapper, IArticle article, IIcon icon, ITag tag)
		{
			_category = category;
			_article = article;
			_icon = icon;
			_mapper = mapper;
			_tag = tag;
		}

		[HttpGet]
		public IEnumerable<CategoryViewModel> GetAll()
		{
			var c = _tag.GetAll();
			var p = _icon.GetAll();
			var g = _mapper.Map<IEnumerable<CategoryDTO>, IEnumerable<CategoryViewModel>>(_category.GetAllWithInclude(x => x.Articles));
			return g;

		}

		[HttpPut]
		[AuthFilter(role: "ContentRedactor")]
		public IActionResult Put(ArticleViewModel art)
		{
			_article.Edit(_mapper.Map<ArticleViewModel, ArticleDTO>(art));
			return Ok();
		}

		[HttpDelete]
		[AuthFilter(role: "ContentRedactor")]
		public IActionResult Delete(Guid articleId)
		{
			_article.Delete(articleId);
			return Ok();
		}

	}
}
