﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Helper;
using AutoMapper;
using Bll.DTOs;
using Bll.Interfaces;
using Mapping.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly ICategory _category;
        private readonly IMapper _mapper;
        private readonly IArticle _article;
        private readonly ITag _tag;

        public ArticleController(ICategory category, IMapper mapper, IArticle article, ITag tag)
        {
            _category = category;
            _article = article;
            _mapper = mapper;
            _tag = tag;
        }

        [HttpGet]
        public IEnumerable<CategoryViewModelIenum> GetAll()
        {
            List<CategoryViewModelIenum> categoryTags = new List<CategoryViewModelIenum>();
            var categories =_mapper.Map<List<CategoryViewModelIenum>>(_category.GetAll());
            foreach (var cat in categories)
            {
                categoryTags.Add(new CategoryViewModelIenum
                {
                    CategoryID = cat.CategoryID,
                    CategoryName = cat.CategoryName
                });
            }
            return categoryTags;
        }

        [HttpPost]
        [AuthFilter(role: "ContentRedactor")]
        public IActionResult Post(ArticleViewModelForCreate art)
        {
            _article.Create(_mapper.Map<ArticleDTO>(art));
            return Ok(art.Name);
        }


		[HttpGet]
		[Route("GetSingle")]
		public IActionResult Get(Guid id)
		{
			var articleView = new ArticleViewModel();
			var c = _tag.GetAll();
			var category = _category.GetAllWithInclude(x => x.Articles);

			foreach (var p in category)
			{
				if (p.Articles.SingleOrDefault(x => x.ArticleID == id) != null)
				{
					articleView = _mapper.Map<ArticleViewModel>(p.Articles.SingleOrDefault(x => x.ArticleID == id));
					articleView.CategoryName = p.Name;
					break;
				}

			}

			return Ok(articleView);
		}

	}
}