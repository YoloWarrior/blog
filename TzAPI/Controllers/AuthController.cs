﻿using API.Helper;
using AutoMapper;
using Bll.DTOs;
using Bll.Interfaces;
using Data.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TzAPI.ViewModel;

namespace API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AuthController : ControllerBase
	{
		IUserService _user;
		IAuth _auth;
		IMapper _mapper;

		public AuthController(IUserService user, IMapper mapper, IAuth auth)
		{
			_user = user;
			_mapper = mapper;
			_auth = auth;
		}

		[HttpGet]
		public IActionResult GetUser(string username)
		{
			var p = _user.Get(username);

			if (p == null)
				return BadRequest("User not found");

			return Ok(p);
		}

		[HttpPost]
		[Route("register")]
		public IActionResult Register([FromBody]UserViewModel user, string password)
		{
			var reguser = "";
			var tokenHandler = new JwtSecurityTokenHandler();

			try
			{
				reguser = _auth.Register(_mapper.Map<UserDTO>(user), password);

			}
			catch (Exception ex)
			{
				return StatusCode(500);
			}

			var token = _auth.Login(user.UserName, password);

			return Ok(new
			{
				token = tokenHandler.WriteToken(token)
			});
		}

		[HttpPost]
		[Route("login")]
		public IActionResult Login(UserViewModelForLogin model)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			var token = _auth.Login(model.UserName, model.Password);

			if (token == null)
				return Unauthorized();

			return Ok(new
			{
				token = tokenHandler.WriteToken(token)
			});
		}

		[HttpGet]
		[Route("checkRole")]
		public IActionResult CheckRole(string username, string role)
		{
			return Ok(_user.CheckRole(username, role));
		}
	}
}
