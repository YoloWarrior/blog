﻿using API.Helper;
using AutoMapper;
using Bll.Auth;
using Bll.Interfaces;
using Bll.Services;
using Data;
using Data.Interfaces;
using Data.Repos;
using Mapping;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace TzAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mapping = new MapperConfiguration(conf =>
            {
                conf.AddProfile(new MappingProfile());
            });

            IMapper map = mapping.CreateMapper();
            services.AddSingleton(map);

            services.AddDbContext<DataContext>(opt =>
            {
                opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                    assemb => assemb.MigrationsAssembly(typeof(DataContext).Assembly.FullName));
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                  .AddJwtBearer(options =>
                  {
                      options.SaveToken = true;
                      options.TokenValidationParameters = new TokenValidationParameters
                      {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            // установка ключа безопасности
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                          // валидация ключа безопасности
                          ValidateIssuerSigningKey = true,
                      };
                  });

            services.AddSession();
            services.AddScoped<IReposWrapper,RepositoryWrapper>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<Bll.Interfaces.IArticle, ArticleService>();
            services.AddScoped<IAuth, AuthService>();
            services.AddScoped<Bll.Interfaces.ICategory, CategoryService>();
            services.AddScoped<Bll.Interfaces.IIcon, IconService>();
            services.AddScoped<Bll.Interfaces.ITag, TagService>();
            services.AddScoped<AuthFilter>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var contentRoot = env.ContentRootPath;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            app.UseStaticFiles();
            app.UseSession();
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
