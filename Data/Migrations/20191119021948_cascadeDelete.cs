﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class cascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tag_Article_ArticleID",
                table: "Tag");

            migrationBuilder.AddForeignKey(
                name: "FK_Tag_Article_ArticleID",
                table: "Tag",
                column: "ArticleID",
                principalTable: "Article",
                principalColumn: "ArticleID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tag_Article_ArticleID",
                table: "Tag");

            migrationBuilder.AddForeignKey(
                name: "FK_Tag_Article_ArticleID",
                table: "Tag",
                column: "ArticleID",
                principalTable: "Article",
                principalColumn: "ArticleID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
