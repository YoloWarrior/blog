﻿using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Repos
{
  public  class TagRepository:Repository<Tag>,ITag
    {
        public TagRepository(DataContext context) : base(context)
        {

        }
    }
}
