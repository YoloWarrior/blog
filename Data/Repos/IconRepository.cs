﻿using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Repos
{
    public class IconRepository : Repository<ArticleIcon>, IIcon
    {
        public IconRepository(DataContext context) : base(context)
        {
        }
    }
}
