﻿using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data.Repos
{
    public class UserRepository : Repository<User>, IUser
    {
        private DataContext _context;
        public UserRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public bool CheckRole(string username,string role)
        {
            var p = _context.Users.SingleOrDefault(x => x.UserName == username && x.Role.RoleName == role);
                return p == null ? false : true;
        }

        public bool LoginExist(string username) => _context.Users.Find(username)==null ? true : false;

    }
}
