﻿using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Repos
{
    public class RepositoryWrapper : IReposWrapper
    {
        private DataContext _db;
        private readonly IArticle _article;
        private readonly IIcon _icon;
        private readonly ITag _tag;
        private readonly IUser _user;
        private readonly ICategory _category;

        public RepositoryWrapper(DataContext context)
        {
            _db = context;
        }


        public IArticle Artice => _article ?? new ArticleRepository(_db);

        public IIcon Icon => _icon ?? new IconRepository(_db);

        public ITag Tag => _tag ?? new TagRepository(_db);

        public IUser User => _user ?? new UserRepository(_db);

        public ICategory Category => _category ?? new CategoryRepository(_db);

        public void Save()
        {
            _db.SaveChanges();  
        }
    }
}
