﻿using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data.Repos
{
    public class CategoryRepository : Repository<Category>, ICategory
    {
        public CategoryRepository(DataContext context) : base(context) { }

    }
}
