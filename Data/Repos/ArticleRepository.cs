﻿using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Repos
{
    public class ArticleRepository : Repository<Article>, IArticle
    {
        public ArticleRepository(DataContext context) : base(context)
        {
        }
    }
}
