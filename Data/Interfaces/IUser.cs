﻿using Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Interfaces
{
   public interface IUser:IGenereticRepos<User>
    {
        bool LoginExist(string email);
        bool CheckRole(string username,string role);
    }
}
