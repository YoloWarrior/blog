﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Data.Interfaces
{
 public   interface IGenereticRepos<T> where T:class
    {
        IEnumerable<T> GetAll();
        void Create(T entity);
        void Delete(Guid Id);   
        void Edit(T entity);
        T Get(Func<T, bool> predicate);
        IEnumerable<T> GetAllWithInclude(params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> FindWithInclude(Func<T, bool> predicate, params Expression<Func<T, object>>[] includeProperties);
    }
}
