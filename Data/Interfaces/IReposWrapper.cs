﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Interfaces
{
   public interface IReposWrapper
    {
        IArticle Artice { get; }
        IIcon Icon { get; }
        ITag Tag { get; }
        IUser User { get; }
        ICategory Category { get; }
        void Save();
    }
}
