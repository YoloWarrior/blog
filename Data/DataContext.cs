﻿using Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data
{
  public  class DataContext:DbContext
    {
    public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { 

        }
        public DataContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=BlogDb;Trusted_Connection=True");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Tag>()
            .HasOne(p => p.Article)
            .WithMany(x => x.Tags)
            .HasForeignKey(x => x.ArticleID)
             .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<Article>()
                .HasOne<ArticleIcon>()
                .WithOne(x => x.Article)
                .HasForeignKey<ArticleIcon>(x => x.ArticleID);


            modelBuilder.Entity<User>()
          .HasOne(p => p.Role)
          .WithMany(x => x.Users)
          .HasForeignKey(x => x.RoleID);

            modelBuilder.Entity<Article>()
               .HasOne(x => x.Category)
               .WithMany(x => x.Articles)
               .IsRequired();

            modelBuilder.Entity<ArticleIcon>()
           .HasOne<Article>()
           .WithOne(x => x.ArticleIcon)
           .HasForeignKey<Article>(x => x.IconID);
        }


    }

}
