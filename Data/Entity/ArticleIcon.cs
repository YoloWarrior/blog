﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entity
{
 public  class ArticleIcon
    {
        [Key]
        public Guid IconID { get; set; }
        public string Path { get; set; }
        public Guid? ArticleID { get; set; }
        public Article Article { get; set; }
    }
}
