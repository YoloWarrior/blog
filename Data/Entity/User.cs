﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entity
{
  public class User
    {
      public Guid UserID { get; set; }
      public string UserName { get; set; }
      public string Password { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Name { get; set; }
      public   string Surname { get; set; }
        public Guid RoleID { get; set; }
        public virtual  Role Role { get; set; }
    }
}
