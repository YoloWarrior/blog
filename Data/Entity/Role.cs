﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entity
{
  public class Role
    {
        [Key]
        public Guid ID { get; set; }
        public string RoleName { get; set; }
        public virtual List<User> Users { get; set; }

    }
}
