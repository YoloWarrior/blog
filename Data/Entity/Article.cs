﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entity
{
  public  class Article
    {
     public Guid ArticleID { get; set; }
     public    string Name { get; set; }
     public   string ShortDescription { get; set; }
     public  string Text { get; set; }
     public  DateTime PublishDate { get; set; }
    public virtual Category Category { get; set; }

     [ForeignKey("ArticleIconID")]
     public Guid? IconID { get; set; }
     public virtual ArticleIcon ArticleIcon { get; set; }
     public virtual IEnumerable<Tag>Tags { get; set; }
    }
}
