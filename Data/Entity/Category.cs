﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entity
{
 public   class Category
    {
        [Key]
        public Guid ID { get; set; }
        public string Name { get; set; }
        public List<Article> Articles { get; set; }
    }
}
