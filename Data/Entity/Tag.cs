﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entity
{
 public  class Tag
    {
     public   Guid TagID { get; set; }
     public   string Name { get; set; }

     public  Guid? ArticleID { get; set; }
      public  Article Article { get; set; }
    }
}
