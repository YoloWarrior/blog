﻿using AutoMapper;
using Bll.DTOs;
using Bll.Interfaces;
using Data;
using Data.Entity;
using Data.Interfaces;
using Data.Repos;
using Mapping.DTOs;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Bll.Auth
{
  public  class AuthService:IAuth
    {
        private IUserService _userService;
        private IReposWrapper _repo;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private DataContext _context;
        public AuthService(IUserService userService,IReposWrapper reposWrapper,IMapper mapper, IConfiguration config, DataContext context)
        {
            _userService = userService;
            _repo = reposWrapper;
            _mapper = mapper;
            _config = config;
            _context = context;
        }
        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                        return false;
                }
            }
            return true;
        }

        private void CreatePasswordHast(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        public SecurityToken Login(string username, string password)
        {
            var p = _userService.Get(username);
            if (p==null)
                return null;
            if (!VerifyPasswordHash(password, p.PasswordHash, p.PasswordSalt))
                return null;
            var claims = new List<Claim>
          {
                new Claim(ClaimTypes.Name,p.UserName)
           };



            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha512Signature);



            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddHours(1),
                SigningCredentials = creds
            };
            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return token;
        }

        public string Register(UserDTO ss, string password)
        {
            try
            {
                ss.UserID = Guid.NewGuid();
               
                CreatePasswordHast(password, out byte[] passwordHash, out byte[] passwordSalt);
                ss.PasswordHash = passwordHash;
                ss.PasswordSalt = passwordSalt;
             ss.RoleID = _context.Roles.SingleOrDefault(x => x.RoleName == "ContentRedactor").ID;
                _repo.User.Create(_mapper.Map<UserDTO,User>(ss));


            }catch (Exception ex) { throw ex; }

            return ss.UserName;
        }
    }
}
