﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bll.Interfaces
{
  public  interface IService<T> where T:class 
    {
        IEnumerable<T> GetAll();
        void Create(T entity);
        void Delete(Guid smth);
        void Edit(T entity);
        T Get(string smth);
        T Get(Guid ID);
    }
}
