﻿using Bll.DTOs;
using Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Bll.Interfaces
{
   public interface IUserService:IService<UserDTO>
    {
        bool UserExist(string username);
      bool  CheckRole(string username, string role);
    }
}
