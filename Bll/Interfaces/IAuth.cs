﻿using Bll.DTOs;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bll.Interfaces
{
   public interface IAuth
    {
        SecurityToken Login(string username, string password);
        string Register(UserDTO ss, string password);
    }
}
