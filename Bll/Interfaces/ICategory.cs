﻿using Mapping.DTOs;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Bll.Interfaces
{
  public  interface ICategory:IService<CategoryDTO>
    {
        IEnumerable<CategoryDTO> GetAllWithInclude(params Expression<Func<CategoryDTO, object>>[] includeProperties);
    }
}
