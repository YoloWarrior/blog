﻿using AutoMapper;
using Bll.DTOs;
using Bll.Interfaces;
using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using ITag = Bll.Interfaces.ITag;

namespace Bll.Services
{
    public class TagService:ITag
    {
        IReposWrapper _repo;
        private readonly IMapper _mapper;
        public TagService(IReposWrapper repo,IMapper mapper) {
            _repo = repo;
            _mapper = mapper;
        }
        public void Create(TagDTO entity)
        {
            try {
                _repo.Tag.Create(_mapper.Map<Tag>(entity));
            }catch(Exception ex) { throw ex; }
            
        }

        public void Delete(Guid name)
        {
            _repo.Tag.Delete(name);
        }

        public void Edit(TagDTO entity)
        {
            try
            {
                _repo.Tag.Edit(_mapper.Map<TagDTO, Tag>(entity));
            }
            catch (Exception ex) { throw ex; }
        }

        public TagDTO Get(string name)
        {
            return _mapper.Map<Tag, TagDTO>(_repo.Tag.Get(x=>x.Name==name));
        }

        public IEnumerable<TagDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<Tag>, IEnumerable<TagDTO>>(_repo.Tag.GetAll());
        }

        public TagDTO Get(Guid ID)
        {
            throw new NotImplementedException();
        }
    }
}
