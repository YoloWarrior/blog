﻿using AutoMapper;
using Bll.Interfaces;
using Data.Entity;
using Data.Interfaces;
using Mapping.DTOs;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using ICategory = Bll.Interfaces.ICategory;

namespace Bll.Services
{
  public  class CategoryService:ICategory
    {
        IReposWrapper _repo;
        IMapper _mapper;
        public CategoryService(IReposWrapper repos, IMapper mapper)
        {
            _repo = repos;
            _mapper = mapper;
        }
        public void Create(CategoryDTO entity)
        {
            _repo.Category.Create(_mapper.Map<Category>(entity));
        }

        public void Delete(Guid smth)
        {
            if (_repo.Category.Get(x => x.ID == smth) != null)
                _repo.Category.Delete(smth);
        }

        public void Edit(CategoryDTO entity)
        {
            _repo.Category.Edit(_mapper.Map<Category>(entity));
        }

        public CategoryDTO Get(string smth)
        {

            var p = _repo.Category.Get(x => x.Name == smth);
            if (p != null)
                return _mapper.Map<CategoryDTO>(p);
            return null;
        }

        public CategoryDTO Get(Guid ID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CategoryDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDTO>>(_repo.Category.GetAll());
        }

        public IEnumerable<CategoryDTO> GetAllWithInclude(params Expression<Func<CategoryDTO, object>>[] includeProperties)
        {
            return _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDTO>>(_repo.Category.GetAllWithInclude(x=>x.Articles));
        }
    }
}
