﻿using AutoMapper;
using Bll.DTOs;
using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using IArticle = Bll.Interfaces.IArticle;

namespace Bll.Services
{
    public class ArticleService : IArticle
    {
        IReposWrapper _repo;
        IMapper _mapper;
        public ArticleService(IReposWrapper repos,IMapper mapper) {
            _repo = repos;
            _mapper = mapper;
        }
        public void Create(ArticleDTO entity)
        {
            _repo.Artice.Create(_mapper.Map<Article>(entity));
        }

        public void Delete(Guid smth)
        {
            if(_repo.Artice.Get(x=>x.ArticleID==smth)!=null)
            _repo.Artice.Delete(smth);
        }

        public void Edit(ArticleDTO entity)
        {
          var article =   _repo.Artice.Get(x => x.ArticleID == entity.ArticleID);
            if(article!=null)
            _repo.Artice.Edit(_mapper.Map<Article>(entity));
        }

        public ArticleDTO Get(Guid smth)
        {

                var p = _repo.Artice.Get(x=>x.ArticleID==smth);
                if(p!=null)
            return _mapper.Map<ArticleDTO>(p);
                 return null;
        }

        public ArticleDTO Get(string smth)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ArticleDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>(_repo.Artice.GetAll());
        }
    }
}
