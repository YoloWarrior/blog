﻿using AutoMapper;
using Bll.DTOs;
using Bll.Interfaces;
using Data.Entity;
using Data.Interfaces;
using Data.Repos;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Bll.Services
{
    public class UserService : IUserService
    {
       private IReposWrapper _repo;
       private readonly IMapper mapper;

        public UserService(IReposWrapper repo,IMapper mapper) {
            _repo = repo;
            this.mapper = mapper;
        }
        public void Create(UserDTO entity)
        {
           
            _repo.User.Create(mapper.Map<User>(entity));
        }
        public void Delete(Guid id)
        {
            if(UserExist(id))
            _repo.User.Delete(id);
        }

        public void Edit(UserDTO entity)
        {
            try
            {
                _repo.User.Edit(mapper.Map<User>(entity));
            }
            catch(Exception ex) { throw ex; }
        }

        public UserDTO Get(Guid id)
        {
            var p = _repo.User.Get(x=>x.UserID ==id);

            return p == null?null: mapper.Map<User,UserDTO>(p);
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return mapper.Map<IEnumerable<User>, IEnumerable<UserDTO>>(_repo.User.GetAll());
        }

        public bool UserExist(Guid id)
        {
           return Get(id) != null ? true : false;
        }

        public bool UserExist(string username)
        {
            return Get(username) != null ? true : false;
        }

        public UserDTO Get(string smth)
        {
            var p = _repo.User.Get(x => x.UserName==smth);

            return p == null ? null : mapper.Map<User, UserDTO>(p);
        }

        public bool CheckRole(string username, string role)
        {
            return _repo.User.CheckRole(username, role);
        }
    }
}
