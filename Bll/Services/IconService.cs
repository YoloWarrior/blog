﻿using AutoMapper;
using Bll.DTOs;
using Bll.Interfaces;
using Data.Entity;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using IIcon = Bll.Interfaces.IIcon;

namespace Bll.Services
{
  public  class IconService : IIcon
    {
        IReposWrapper _repo;
        IMapper _mapper;
        public IconService(IReposWrapper repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        public void Create(IconDTO entity)
        {
            try
            {
                _repo.Icon.Create(_mapper.Map<IconDTO, ArticleIcon>(entity));
            }
            catch (Exception ex) { throw ex; }
        }

        public void Delete(Guid name)
        {
            if (_repo.Icon.Get(x=>x.IconID==name) != null)
                _repo.Icon.Delete(name);
        }

        public void Edit(IconDTO entity)
        {
            try
            {
                _repo.Icon.Edit(_mapper.Map<IconDTO, ArticleIcon>(entity));
            }
            catch (Exception ex) { throw ex; }
        }

        public IconDTO Get(string name)
        {
            var p = _repo.Icon.Get(x=>x.Path==name);

            return p == null ? null : _mapper.Map<IconDTO>(p);
        }

        public IconDTO Get(Guid ID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IconDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<ArticleIcon>, IEnumerable<IconDTO>>(_repo.Icon.GetAll());
        }
    }
}